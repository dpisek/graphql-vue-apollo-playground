import ParcelProxyServer from "parcel-proxy-server";

// configure the proxy server
const server = new ParcelProxyServer({
  entryPoint: "./src/index.html",
  proxies: {
    // add proxies here
    "/api": {
      target: "http://localhost:4000",
    },
  },
});

// the underlying parcel bundler is exposed on the server
// and can be used if needed
server.bundler.on("buildEnd", () => {
  console.log("Build completed!");
});

// start up the server
server.listen(8080, () => {
  console.log(`Parcel proxy server has started: http://localhost:8080`);
});
